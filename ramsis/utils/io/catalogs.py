import logging
from datetime import datetime
from io import BytesIO
from typing import Union

import numpy as np
import pandas as pd
from obspy import read_events
from obspy.core.event import Catalog, Event, Magnitude, Origin

from ramsis.utils.io.coordinates import CoordinateTransformer

logger = logging.getLogger(__name__)


def catalog_to_df(
        catalog: Union[Catalog, BytesIO],
        proj: str,
        reference_point: tuple = (0, 0, 0),
        starttime: datetime = datetime(1970, 1, 1),
        endtime: datetime = None) -> pd.DataFrame:
    """
    Read catalog to dataframe and convert to local ENU coordinate system.

    :param catalog: file name of catalog file or obspy catalog object.
    :param proj: proj string of local crs.
    :param reference_poin: reference point for local crs.
    :param starttime: only events after this timestamp, count from here.
    :param endtime: only retrieve events before this timestamp.

    :rtype: pandas.DataFrame
    """
    try:
        if isinstance(catalog, BytesIO):
            obspy_catalog = read_events(catalog)
        elif isinstance(catalog, Catalog):
            obspy_catalog = catalog
        else:
            raise ValueError('Could not read catalog, unknown format.')
    except Exception as e:
        raise ValueError(
            f'Error parsing "seismic_catalog", failed with error: {e}')

    if starttime:
        obspy_catalog = obspy_catalog.filter(
            f'time > {starttime.strftime("%Y-%m-%dT%H:%M:%S")}')
    if endtime:
        obspy_catalog = obspy_catalog.filter(
            f'time < {endtime.strftime("%Y-%m-%dT%H:%M:%S")}')

    try:
        if len([e for e in obspy_catalog.events
                if e.preferred_magnitude()
                and e.preferred_origin()]) <= 0:
            logger.warning(
                'No events with a preferred magnitude and origin found. '
                'No recorded events are provided to the model.')
            return pd.DataFrame(
                columns=['Xs', 'Ys', 'Zs', 'mag'], index=pd.DatetimeIndex([]))

        (dttime_column,
            ts_column,
            mag_column,
            latitude_column,
            longitude_column,
            depth_column) = zip(*[(e.preferred_origin().time.datetime,
                                e.preferred_origin().time.datetime.timestamp(),
                                e.preferred_magnitude().mag,
                                e.preferred_origin().latitude,
                                e.preferred_origin().longitude,
                                e.preferred_origin().depth)
                                for e in obspy_catalog.events
                                if e.preferred_magnitude()
                                and e.preferred_origin()])
    except Exception as e:
        raise ValueError(f'The catalog is not valid, reading failed with: {e}')

    transformer = CoordinateTransformer(proj, *reference_point)

    Xs, Ys, Zs = transformer.to_local_coords(
        longitude_column, latitude_column, np.array(depth_column) * -1)

    ts = [(e - starttime).total_seconds() for e in dttime_column]

    # sort_index() is required for the model. If the index is not
    # in order, it cannot be searched and sliced.
    df_catalog = pd.DataFrame({'ts': ts,
                               'mag': mag_column,
                               'Xs': Xs,
                               'Ys': Ys,
                               'Zs': Zs,
                               'Lat': latitude_column,
                               'Lon': longitude_column,
                               'depth': depth_column},
                              index=dttime_column).sort_index()

    logger.info(f'Catalog has {len(df_catalog)} objects.')

    return df_catalog


def df_to_catalog(
        catalog_df: pd.DataFrame,
        proj: str,
        reference_point: tuple = (0, 0, 0)) -> Catalog:
    """
    Convert catalog dataframe into obspy format.

    :param catalog: Catalog dataframe with coordinates in ENU.
    :param proj: Proj string of CRS in which the coordinates are provided.
    :param reference_point: reference point in to which the coordinates are
           provided in ENU.

    :rtype: obspy catalog
    """

    # check dataframe
    cols = ['Xs', 'Ys', 'Zs', 'mag']
    if not isinstance(catalog_df.index, pd.DatetimeIndex) or \
            not all(c in catalog_df.columns for c in cols):
        raise KeyError(
            'Dataframe catalog has missing columns, required are '
            f'{cols} and a DatetimeIndex, got {catalog_df.columns} '
            f'and a {type(catalog_df.index)} index.')

    transformer = CoordinateTransformer(proj, *reference_point)
    longitude, latitude, altitude = transformer.from_local_coords(
        catalog_df.Xs.values,
        catalog_df.Ys.values,
        catalog_df.Zs.values)

    cat = Catalog()

    for i in range(len(catalog_df)):
        e = Event()
        orig = Origin()
        orig.time = catalog_df.index[i]
        orig.latitude = latitude[i]
        orig.longitude = longitude[i]
        orig.depth = -altitude[i]
        e.origins.append(orig)
        e.preferred_origin_id = orig.resource_id

        magn = Magnitude()
        magn.mag = catalog_df.mag.values[i]
        magn.magnitude_type = 'Mw'
        e.magnitudes.append(magn)
        e.preferred_magnitude_id = magn.resource_id

        cat.append(e)

    return cat
