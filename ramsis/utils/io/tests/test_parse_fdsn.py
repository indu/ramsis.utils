import os

import numpy as np
import pandas as pd
from obspy import Catalog, read_events
from ramsis.utils.io import catalogs

DIRPATH = os.path.dirname(os.path.abspath(__file__))


def get_origin(event):
    lat = event.preferred_origin().latitude
    lon = event.preferred_origin().longitude
    depth = event.preferred_origin().depth
    return (lat, lon, depth)


def test_catalog_to_df():
    """ Catalog conversion into DataFrame with coordinate transformation
        into local CRS.
    """

    qml_catalog = read_events(os.path.join(
        DIRPATH, "data", "bedretto_nov_catalog.qml"))

    df_catalog = catalogs.catalog_to_df(qml_catalog,
                                        'epsg:2056', (2000000, 1000000, 0))
    assert isinstance(df_catalog, pd.DataFrame)

    df_values = np.array(df_catalog.iloc[[3]][['Xs', 'Ys', 'Zs']])
    target_values = np.array([[679494.679042, 151465.098437, 1361.]])

    np.testing.assert_allclose(df_values, target_values)


def test_df_to_catalog():
    """ DataFrame conversion to obspy Catalog with coordinate transformation
        from local CRS
    """

    catalog_df = pd.read_hdf(os.path.join(
        DIRPATH, 'data', 'TrainingTarget.h5'))
    catalog = catalogs.df_to_catalog(
        catalog_df, 'epsg:2056', (2679720.696, 1151600.128, 1480))

    assert isinstance(catalog, Catalog)

    target_values = (46.511128126363225, 8.47737124530586, -1502.66896847305)

    np.testing.assert_allclose(get_origin(catalog.events[0]), target_values)


def test_both_ways():
    """ Assert that converting to and from DataFrame results in no change
        in coordinates.
    """

    qml_catalog = read_events(os.path.join(
        DIRPATH, "data", "bedretto_nov_catalog.qml"))

    df_catalog = catalogs.catalog_to_df(
        qml_catalog, 'epsg:2056', (2000000, 1000000, 0))

    final_catalog = catalogs.df_to_catalog(
        df_catalog, 'epsg:2056', (2000000, 1000000, 0))

    comparison_event = next(
        e for e in final_catalog.events if
        e.preferred_origin().time.datetime
        == qml_catalog.events[1].preferred_origin().time.datetime)

    start_values = get_origin(qml_catalog.events[1])
    end_values = get_origin(comparison_event)

    np.testing.assert_allclose(start_values, end_values)
