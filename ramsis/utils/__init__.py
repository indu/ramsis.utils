# Copyright 2018, ETH Zurich - Swiss Seismological Service SED
"""
General purpose ramsis utilities
"""

import argparse
import os

# -----------------------------------------------------------------------------


def realpath(p):
    return os.path.realpath(os.path.expanduser(p))


def real_file_path(path):
    """
    check if file exists
    :returns: realpath in case the file exists
    :rtype: str
    :raises argparse.ArgumentTypeError: if file does not exist
    """
    path = realpath(path)
    if not os.path.isfile(path):
        raise argparse.ArgumentTypeError(
            '{0!r} is not a valid file path.'.format(path))
    return path
