# # Copyright 2019, ETH Zurich - Swiss Seismological Service SED
# """
# Testing facilities for borehole/hydraulics data import.
# """

# import datetime
# import os
# import unittest

# from unittest import mock
# import requests
# from ramsis.utils.clients import binary_request

# RAMSIS_PROJ = ("+proj=utm +zone=32 +ellps=WGS84 +datum=WGS84 i"
#                "+units=m +x_0=0.0 +y_0=0.0 +no_defs")
# WGS84_PROJ = 4326
# REFERENCE_X = 681922
# REFERENCE_Y = 1179229


# TEST COPIED OVER FROM RAMSIS.DATAMODEL FOR BINARY_REQUEST
# class HYDWSBoreholeHydraulicsDeserializerTestCase(unittest.TestCase):
#     """
#     Test for the
#     :py:class:`RAMSIS.io.hydraulics.HYDWSBoreholeHydraulicsDeserializer`
#                class.
#     """
#     PATH_RESOURCES = os.path.join(os.path.dirname(os.path.abspath(__file__)),
#                                   'resources')
#     maxDiff = None

#     @mock.patch('requests.get')
#     def test_with_binary_request(self, mock_req):

#         def create_mock_resp():
#             m = mock.MagicMock()
#             p = mock.PropertyMock(return_value=200)
#             type(m).status_code = p

#             with open(os.path.join(self.PATH_RESOURCES, 'hyd.json'),
#                       'rb') as ifs:
#                 p = mock.PropertyMock(return_value=ifs.read())
#                 type(m).content = p

#             return m

#         mock_req.return_value = create_mock_resp()

#         url = ("http://foo.bar.com/v1/borehole/"
#                "c21pOmNoLmV0aHouc2VkL2JoLzExMTExMTExLWU0YTAtNDY5Mi1iZjI5LTMzY"
#                "jU1OTFlYjc5OA==")
#         req_params = {
#             'starttime': '2019-01-01T00:00:00',
#             'endtime': '2020-01-01T00:00:00',
#             'format': 'json', }

#         deserializer = HYDWSBoreholeHydraulicsDeserializer(
#             ramsis_proj=RAMSIS_PROJ,
#             external_proj=WGS84_PROJ,
#             ref_easting=REFERENCE_X,
#             ref_northing=REFERENCE_Y,
#             transform_func_name='pyproj_transform_to_local_coords')

#         with binary_request(requests.get, url, req_params) as ifs:
#             bh = deserializer.load(ifs)

#         self.assertEqual(len(bh.sections), 1)
#         self.assertEqual(
#             bh.publicid,
#             'smi:ch.ethz.sed/bh/11111111-e4a0-4692-bf29-33b5591eb798')

#         bh_section = bh.sections[0]
#         # borehole section coordinates
#         # The location of the borehole should be at the
#         # reference point.
#         self.assertEqual(int(bh_section.topx_value), 0)
#         self.assertEqual(int(bh_section.topy_value), 0)
#         self.assertEqual(int(bh_section.topz_value), 100)
#         self.assertEqual(int(bh_section.bottomx_value), 0)
#         self.assertEqual(int(bh_section.bottomy_value), 0)
#         self.assertEqual(int(bh_section.bottomz_value), -900)
#         # additional borehole section attributes
#         self.assertEqual(len(bh_section.hydraulics.samples), 2)
#         self.assertEqual(
#             bh_section.publicid,
#             'smi:ch.ethz.sed/bh/section/11111111-8d89-4f13-95e7-526ade73cc8b')
#         self.assertEqual(bh_section.holediameter_value, 0.3)

#         # briefly validate samples
#         s0 = bh_section.hydraulics.samples[0]
#         self.assertEqual(s0.datetime_value,
#                          datetime.datetime(2019, 5, 3, 13, 27, 9, 117623))
#         s1 = bh_section.hydraulics.samples[1]
#         self.assertEqual(s1.datetime_value,
#                          datetime.datetime(2019, 5, 3, 15, 27, 9, 117623))
