# ramsis.utils

Utilities to work with data connected to RT-Ramsis, SED and Induced Seismicity. This repository can be used as is for custom code. It is also used however in future operational processes like RT-Ramsis and Live logging of data.

### Under Development

This repository and the connected data sources are still under development. While it is still possible that breaking changes will be introduced on both sides (code and data), we aim to keep them always compatible with each other.

## Installation:

It is always advised to use a virtual environment when developing with python. The following installation instruction is just one of several methods to install this repository in a local development environment:

```
python3 -m venv env
source env/bin/activate
pip install -U wheel setuptools pip

# the following command installs this repository
# after the @ it is possible to either specify the branch, or a version (eg. '@0.2.0')
pip install git+https://gitlab.seismo.ethz.ch/indu/ramsis.utils.git@master
```

## Examples:

If you've installed the repository like described above, it is easiest now to copy over the example code found in the `examples` directory and run it while the virtual environment is activated. If you've installed from source (first cloned the repository), you can directly run it by providing the path.

```
source env/bin/activate
python path/to/examples/hydraulics.py
```

# Documentation

## HYDWS Utility Classes

Two classes to work with hydraulic data are available. The class documentation is done pretty thouroughly including typehints. Therefore the IDE tooltips together with the examples in `sfm_models/models/example/run_model.py` as well as the list of available methods here should make it easy to work with these classes.

### HYDWSDataSource

Retrieve hydraulic data from a HYDWebService. When initializing the class the URL of the Webservice must be passed directly.

#### Available Methods:

-   `list_boreholes()`: Returns a list of all boreholes including their metadata and sections.
-   `get_borehole_metadata(borehole_id)`: returns metadata for a specific borehole with all metadata of its sections.
-   `get_section(borehole_id, section_id, starttime, endtime)`: Returns complete borehole data together with the requested section and its hydraulic data in the specified time span.
-   `get_section_hydraulics(borehole_id, section_id, starttime, endtime)`: Same as above but only returns the list with the hydraulic data.

### HYDWSParser

Transforms data between HYDWS Json (python dictionaries) and Dataframes.

#### Available Methods:

-   `create_empty_borehole(number_of_sections)`: Creates a borehole with dummy metadata.
-   `create_borehole_from_config()`: Create a parser object with metadata calculated from config files.
-   `load_metadata(metadata)`: Save metadata to the class object. Passing along data for the same borehole merges the sections and updates the rest of the data. Passing data for a new borehole overwrites it.
-   `get_metadata()`: Retrieve a copy of all metadata saved at the moment in the parser object.
-   `load_hydraulics_dataframe(section_id, dataframe, merge=False)`: Save hydraulic data to class object from a dataframe. Default merge=False means that data is completely replaced.
-   `load_hydraulics_json(section_id, json_data)`: Save hydraulic data to class object from a json dictionary.
-   `load_borehole_json(section_id, json_data)`: Save data of a borehole to class object from a json dictionary.
-   `get_hydraulics_dataframe(section_id, date)`: Return hydraulic data as a dataframe for `section_id` and starting at `date`.
-   `get_hydraulics_json(section_id, date)`: Return hydraulic data as a json dict for `section_id` and starting at `date`.
-   `get_borehole_json(date)`: Return borehole data as a json dict with hydraulic data starting at `date`.

### Coordinate Transformer

Utility which can be used to transform coordinates between WGS84 and a local cartesian system with or without an offset.

#### Available Methods:

-   **init**: `CoordinateTransformer(local_proj, ref_easting=0.0, ref_northing=0.0, ref_depth=0.0, external_proj=4326)`: Any EPSG code or proj4 string can be used for local_proj, external_proj default is WGS84
-   `to_local_coords(lon, lat, altitude=None)`: Transforms from the external projection to the local one including the offset.
-   `from_local_coords(easting, northing, altitude=None)`: Transforms from the local coordinates to an external one taking the offset into account.

### RawHydraulicsParser

Slightly more complex utility to create populated HYDWSParser objects reading a DataFrame according to a config file. Usage see `examples/parse_data.py`.
